import React, {useState} from 'react';
import {createMuiTheme, MuiThemeProvider} from '@material-ui/core/styles';
import {Theme} from '@material-ui/core/styles/createMuiTheme';
import {makeStyles, createStyles} from '@material-ui/core/styles';
import {blue, grey} from "@material-ui/core/colors";
import {Container, CssBaseline, Grid, TextField, IconButton, List, ListItem, ListItemIcon, Checkbox, Snackbar, Typography, Link} from '@material-ui/core';
import SendIcon from '@material-ui/icons/Send';
import DoneIcon from '@material-ui/icons/Done';
import DoneAllIcon from '@material-ui/icons/DoneAll';
import DeleteIcon from '@material-ui/icons/Delete';
import ClearAllIcon from '@material-ui/icons/ClearAll';
import MuiAlert from '@material-ui/lab/Alert';

const theme: Theme = createMuiTheme({
    palette: {
        type: 'dark',
        primary: {
            main: blue[500]
        }
    }
});

const useStyles = makeStyles((theme: Theme) => createStyles({
    root: {
        padding: '50px'
    },
    source: {
        paddingBottom: '20px'
    },
    addItem: {
        alignItems: 'center'
    },
    form: {
        alignItems: 'flex-end',
        marginBottom: '10px'
    },
    formInput: {
        marginBottom: theme.spacing(1)
    },
    todoListItem: {
        padding: '10px 0'
    },
    emptyListMessage: {
        display: 'inline-block',
        color: grey[600]
    },
}));

export default (): JSX.Element => {
    const [todoList, setTodoList] = useState<string[]>([]);
    const [completedList, setCompletedList] = useState<string[]>([]);
    const [todoItem, setTodoItem] = useState<string>('');
    const [checked, setChecked] = useState<number[]>([]);
    const [message, setMessage] = useState<string>('');
    const classes = useStyles();

    interface handleSubmit {
        (event: React.FormEvent<HTMLFormElement>): void;
    }

    interface handleCheck {
        (value: number | void): (() => void) | undefined;
    }

    interface processItem {
        (index: number): (() => void);
    }

    interface voidFunction {
        (): void;
    }

    const handleSubmit: handleSubmit = event => {
        event.preventDefault();

        if (todoItem) {
            setTodoList(prevState => [todoItem, ...prevState]);
            setTodoItem('');
            setChecked(prevState => prevState.map(prevChecked => prevChecked + 1));
        }
    }

    const handleCheck: handleCheck = value => () => {
        if (typeof value === 'number') {
            if (checked.includes(value)) {
                setChecked(prevState => prevState.filter(prevItem => prevItem !== value));
            } else {
                setChecked([...checked, value]);
            }
        } else {
            setChecked(prevChecked => prevChecked.length ? [] : [...todoList.keys()]);
        }
    };

    const removeTodoItem: processItem = index => () => {
        setTodoList(prevState => prevState.filter((prevItem, prevIndex) => index !== prevIndex));
        setChecked((prevCheckedState: number[]): number[] => prevCheckedState
            .filter((prevChecked: number): boolean => prevChecked !== index)
            .map((prevChecked: number): number => {
                if (prevChecked > index) {
                    return prevChecked - 1;
                } else {
                    return prevChecked;
                }
            })
        );
    }

    const completeItem: processItem = index => () => {
        removeTodoItem(index)();
        setCompletedList([todoList[index], ...completedList]);
    };

    const removeTodoItems: voidFunction = () => {
        if (Boolean(checked.length)) {
            if (checked.length === todoList.length) {
                setTodoList([]);
                setChecked([]);
                setMessage('Deleted all todo list items');
            } else {
                setTodoList(prevState => prevState.filter((prevItem, prevIndex) => !checked.includes(prevIndex)));
                setChecked([]);
                setMessage(`Deleted ${checked.length} todo list items`);
            }
        }
    };

    const completeTodoItems: voidFunction = () => {
        if (Boolean(checked.length)) {
            if (checked.length === todoList.length) {
                setCompletedList([...todoList, ...completedList]);
                setTodoList([]);
                setChecked([]);
                setMessage('Completed all todo list items');
            } else {
                setCompletedList([...todoList.filter((todoItem: string, index: number) => checked.includes(index)), ...completedList]);
                setTodoList(prevState => prevState.filter((todoItem, index) => !checked.includes(index)));
                setChecked([]);
                setMessage(`Completed ${checked.length} todo list items`);
            }
        }
    };

    return (
        <MuiThemeProvider theme={theme}>
            <CssBaseline/>
            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                }}
                open={Boolean(message)}
                autoHideDuration={4000}
                onClose={() => setMessage('')}
            >
                <MuiAlert elevation={6} variant="filled" onClose={() => setMessage('')} severity="success">
                    {message}
                </MuiAlert>
            </Snackbar>
            <Container maxWidth="md" className={classes.root}>
                <Typography className={classes.source}>
                    <Link href="https://gitlab.com/cellsheet/todo" target="_blank" rel="noopener">
                        https://gitlab.com/cellsheet/todo
                    </Link>
                </Typography>
                <h1>Todo List</h1>
                <form onSubmit={handleSubmit}>
                    <Grid className={classes.form} container spacing={2}>
                        <Grid item xs>
                            <TextField
                                fullWidth
                                label="Todo Item"
                                variant="standard"
                                onChange={event => setTodoItem(event.target.value)}
                                value={todoItem}
                            />
                        </Grid>
                        <Grid item>
                            <IconButton type="submit" aria-label="submit">
                                <SendIcon fontSize="inherit"/>
                            </IconButton>
                        </Grid>
                    </Grid>
                </form>
                <h2>Todo</h2>
                <List>
                    {
                        todoList.length ? (
                            <>
                                <ListItem className={classes.todoListItem} divider>
                                    <Grid container>
                                        <Grid item xs>
                                            <ListItemIcon>
                                                <Checkbox
                                                    onClick={handleCheck()}
                                                    checked={Boolean(checked.length) && checked.length === todoList.length}
                                                    indeterminate={Boolean(checked.length) && checked.length !== todoList.length}
                                                    inputProps={{'aria-labelledby': 'todo-item-all'}}
                                                />

                                            </ListItemIcon>
                                        </Grid>
                                        <Grid item>
                                            <IconButton
                                                onClick={removeTodoItems}
                                                aria-label="delete"
                                            >
                                                <ClearAllIcon fontSize="inherit"/>
                                            </IconButton>
                                        </Grid>
                                        <Grid item>
                                            <IconButton
                                                onClick={completeTodoItems}
                                                aria-label="done"
                                            >
                                                <DoneAllIcon fontSize="inherit"/>
                                            </IconButton>
                                        </Grid>
                                    </Grid>
                                </ListItem>
                                {todoList.map((todoItem: string, index: number) => (
                                    <ListItem key={index} className={classes.todoListItem} divider>
                                        <Grid container>
                                            <Grid item>
                                                <ListItemIcon>
                                                    <Checkbox
                                                        onClick={handleCheck(index)}
                                                        checked={checked.includes(index)}
                                                        inputProps={{'aria-labelledby': 'todo-item-' + index}}
                                                    />
                                                </ListItemIcon>
                                            </Grid>
                                            <Grid item xs>
                                                <p>{todoItem}</p>
                                            </Grid>
                                            <Grid item>
                                                <IconButton
                                                    onClick={removeTodoItem(index)}
                                                    aria-label="delete"
                                                >
                                                    <DeleteIcon fontSize="inherit"/>
                                                </IconButton>
                                            </Grid>
                                            <Grid item>
                                                <IconButton
                                                    onClick={completeItem(index)}
                                                    aria-label="done"
                                                >
                                                    <DoneIcon fontSize="inherit"/>
                                                </IconButton>
                                            </Grid>
                                        </Grid>
                                    </ListItem>
                                ))}
                            </>
                        ) : <ListItem component="i" className={classes.emptyListMessage}>No todo items</ListItem>
                    }
                </List>
                <h2>Completed</h2>
                <List>
                    {
                        completedList.length ? (
                            completedList.map((completedItem: string, index: number) => (
                                <ListItem key={index} divider>
                                    <p>{completedItem}</p>
                                </ListItem>
                            ))
                        ) : <ListItem component="i" className={classes.emptyListMessage}>No completed items</ListItem>
                    }
                </List>
            </Container>
        </MuiThemeProvider>
    );
};
